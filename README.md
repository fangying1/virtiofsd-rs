# virtiofsd-rs

A [virtio-fs](https://virtio-fs.gitlab.io/) vhost-user device daemon
written in Rust.
